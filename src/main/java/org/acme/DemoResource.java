package org.acme;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.resteasy.reactive.RestQuery;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;

@Path("/test")
public class DemoResource {
    final String SITE_ADDRESS = "https://masothue.com";
    final String MST = "0313950909";
    final ObjectMapper mapper = new ObjectMapper();
    final String[] ATT_NAMES = {
            "Mã số thuế",
            "Tên quốc tế",
            "Tên viết tắt",
            "Địa chỉ",
            "Ngày hoạt động",
            "Quản lý bởi",
            "Loại hình DN",
            "Tình trạng"
    };
    final SecureRandom secureRandom = new SecureRandom();

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Map<String, String> hello(@RestQuery String masothue) throws IOException {
        String url = connectAndGetCompanyUrl(masothue);
        Document doc = Jsoup.connect(SITE_ADDRESS + url).get();
        Element tableInfo = doc.selectFirst("table.table-taxinfo");
//        System.out.println(tableInfo);
        final Map<String, String> mapRes = new HashMap<>();
        mapRes.put("Tên", tableInfo.selectFirst("table > thead > tr > th").text());
        for (String name: ATT_NAMES){
            Element element = tableInfo.selectFirst("td:contains("+name+")");
            if (element != null){
                mapRes.put(name, element.parent().select("td").get(1).text());
            }
        }
        return mapRes;
    }

    private String randomCode(){

        String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        StringBuilder generatedString= new StringBuilder();
        for (int i = 0; i < 5; i++) {
            int randonSequence = secureRandom.nextInt(CHARACTERS.length());
            generatedString.append(CHARACTERS.charAt(randonSequence));
        }
        return generatedString.toString().toLowerCase();
    }

    private String connectAndGetCompanyUrl(String masothue) throws IOException {
        final Connection.Response response1 = Jsoup.connect(SITE_ADDRESS + "/Ajax/Token")
                .data("r", randomCode())
                .method(Connection.Method.POST)
                .execute();
        final Map<String, String> map = mapper.readValue(response1.body(), Map.class);

        final Map<String, String> formData = new HashMap<>();
        formData.put("type", "auto");
        formData.put("q", masothue != null ? masothue : MST);
        formData.put("force-search", "1");
        formData.put("token", map.get("token"));

        final Map<String, String> header = new HashMap<>();
        header.put("scheme", "https");
        header.put("X-Requested-With", "XMLHttpRequest");
        header.put("origin", SITE_ADDRESS);
        header.put("referer", SITE_ADDRESS);
        header.put("user-agen", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36");

        final Connection.Response searchResponse = Jsoup.connect(SITE_ADDRESS + "/Ajax/Search")
                .method(Connection.Method.POST)
                .cookies(response1.cookies())
                .data(formData)
                .headers(header)
                .execute();

        final Map<String, String> mapSearchResponse = mapper.readValue(searchResponse.body(), Map.class);
        return mapSearchResponse.get("url");
    }
}